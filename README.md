Caltech Physics 12c: Statistical Mechanics, Spring 2019
=======================================================

[Course website](http://theory.caltech.edu/~dsd/ph12c/2019/)

These lecture notes are not original. They draw from
[Kittel and Kroemer](https://www.amazon.com/Thermal-Physics-2nd-Charles-Kittel/dp/0716710889),
[David Tong's notes on statistical physics](http://www.damtp.cam.ac.uk/user/tong/statphys.html),
[John Preskill's notes for past iterations of Ph 12c](http://www.theory.caltech.edu/~preskill/ph12c/),
and possibly other unattributed sources.
